---
layout: post
title:  "CompSciTR Blog"
date:   2017-11-08 00:55:14 -400
---

# Bilgisayar Bilimi Türkçe Kaynak Çalışması 

![logo](http://www.imgim.com/glendaplan9compscitr.png "Glenda")



### Bu çalışma genel olarak:
Bilgisayar bilimini de tıpkı fizik (Evren - Einstein), kimya, biyoloji(Evrim) ve diğer pozitif bilimler gibi popüler bilim dünyasında yer edindirmeyi amaçlayıp; Bu alanda meraklı amatör kimseler için kaynak niteliğinde çalışmalar ortaya koymayı hedeflemektedir.

Öncelikli amaç, türkçe neredeyse hiç kaynak bulunmayan konular hakkında ingilizce yayınlanmış çalışmalardan direk veya özet çeviri. Sonraları var olan çeviri veya kaynak niteliğinde olan çalışmalardan outdate (güncelliğini kaybetmiş) olan, eksik veya yanlış bilgi içeren çalışmalara alternatif oluşturmak.

Şuan da üzerinde çalışılması planlanan konu ve makaleler:

 * The Status of the P Versus NP Problem (Teorik - 2009)
 * Anonymous function (Lambda calculus) 
 * Bilgisayar Bilimi başlığının ansiklopedik değerlendirilmesi. 


Bu repository (havuz) yayınlanmış veya üzerinde uzun zaman çalışılması gereken makale ve başlıkların okunabilir alt başlıklarının yayınlandığı yerdir. Yazım hataları veya içerik hakkında ki herhangi bir problem için issue açabilirsiniz. (gitlab hesabınız yoksa mail atabilirsiniz.)